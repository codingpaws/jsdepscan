## jsdepscan

Scans `package.json` and `yarn.lock` to find major, minor, and patch
updates to the `dependencies` (and later `devDependencies`) listed in
`package.json`. It also reports stale dependencies, i.e. those where the
latest version was published over a year (365 days) ago.
