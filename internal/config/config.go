package config

import (
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"slices"

	"github.com/gobwas/glob"
	"gopkg.in/yaml.v3"
)

type pattern struct {
	glob.Glob
	original string
}

type ymlConfig struct {
	Rules []struct {
		Name   pattern
		Reason string
		Ignore []string
	}
}

type Config struct {
	conf ymlConfig
}

var _ yaml.Unmarshaler = (*pattern)(nil)

func (self *pattern) UnmarshalYAML(data *yaml.Node) error {
	if data.Tag != "!!str" {
		return fmt.Errorf("invalid pattern: %s", data.Value)
	}

	glob, err := glob.Compile(data.Value)

	if err != nil {
		return fmt.Errorf("invalid pattern at %d:%d: %w", data.Line, data.Column, err)
	}
	*self = pattern{
		Glob:     glob,
		original: data.Value,
	}
	return nil
}

func (self Config) Ignores(dep string, category string) (string, bool) {
	for _, ignore := range self.conf.Rules {
		isCategory := slices.Contains(ignore.Ignore, category) || len(ignore.Ignore) == 0

		if isCategory && ignore.Name.Match(dep) {
			return ignore.Reason, true
		}
	}
	return "", false
}

func LoadConfigFromDefault() (*Config, error) {
	f, err := os.Open(".jsdepscan.yml")
	if errors.Is(err, os.ErrNotExist) {
		log.Println(".jsdepscan.yml not found, using defaults")
		return &Config{}, nil
	}
	if err != nil {
		return nil, err
	}
	defer f.Close()
	conf, err := LoadConfig(f)
	if err != nil {
		return nil, err
	}
	for _, rule := range conf.conf.Rules {
		if rule.Reason == "" {
			log.Fatalf("rule for '%s' must have a reason", rule.Name.original)
		}
	}
	return conf, nil
}

func LoadConfig(reader io.Reader) (conf *Config, err error) {
	var innerConf *ymlConfig
	err = yaml.NewDecoder(reader).Decode(&innerConf)
	if err != nil {
		return
	}
	conf = &Config{
		conf: *innerConf,
	}
	return
}
