package registry

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/Masterminds/semver"
)

type registryVersionInfo struct {
	Name          string          `json:"name"`
	LatestVersion *semver.Version `json:"version"`
	Deprecated    string          `json:"deprecated"`
}

type VersionInfo struct {
	Version    *semver.Version
	Deprecated string
}

// Incomplete but we only need this for now
type PackageInfo struct {
	Name          string
	LatestVersion *semver.Version
	Versions      []VersionInfo
	PublishedAt   time.Time
}

func FetchLatestPackageInfo(dep string) (info *PackageInfo, err error) {
	resp, err := http.Get("https://registry.npmjs.org/" + dep)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return nil, fmt.Errorf("unexpected status code: %d", resp.StatusCode)
	}

	var packageDef struct {
		Versions map[string]registryVersionInfo `json:"versions"`
		DistTags struct {
			Latest string `json:"latest"`
		} `json:"dist-tags"`
		Time map[string]time.Time `json:"time"`
	}

	err = json.NewDecoder(resp.Body).Decode(&packageDef)
	if err != nil {
		return nil, fmt.Errorf("parsing package definition from registry: %w", err)
	}

	latestInfo, ok := packageDef.Versions[packageDef.DistTags.Latest]
	if !ok {
		fmt.Println("latest =", packageDef.DistTags.Latest)
		for key := range packageDef.Versions {
			fmt.Println(key)
		}
		return nil, fmt.Errorf("no latest version found")
	}

	info = &PackageInfo{
		Name:          latestInfo.Name,
		LatestVersion: latestInfo.LatestVersion,
		PublishedAt:   packageDef.Time[packageDef.DistTags.Latest],
	}

	for rawVersion, v := range packageDef.Versions {
		version, err := semver.NewVersion(rawVersion)
		if err != nil {
			return nil, fmt.Errorf("parsing version %s: %w", version, err)
		}
		info.Versions = append(info.Versions, VersionInfo{
			Version:    version,
			Deprecated: v.Deprecated,
		})
	}

	return
}
