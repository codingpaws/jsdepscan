package depinfo

import (
	"fmt"
	"slices"
	"time"

	"gitlab.com/codingpaws/jsdepscan/internal/registry"
	"gitlab.com/codingpaws/yarnlock"

	"github.com/Masterminds/semver"
)

type Request struct {
	Alias           string
	PackageName     string
	SelectedVersion string
	RawVersion      string
}

type Status struct {
	CurrentVersion, LatestVersion         *semver.Version
	LatestPublishedAt                     time.Time
	LatestMajor, LatestMinor, LatestPatch *semver.Version
	LatestDeprecated                      string
	LatestNotDeprecatedVersion            *semver.Version
}

type Resolver struct {
	lockfile *yarnlock.File
}

func NewResolver(lockfile *yarnlock.File) *Resolver {
	return &Resolver{lockfile}
}

func (self Resolver) Resolve(req Request) (status *Status, err error) {
	status = &Status{}
	info, err := registry.FetchLatestPackageInfo(req.PackageName)
	if err != nil {
		return nil, fmt.Errorf("fetching dependency info: %w", err)
	}
	lockedVersion, err := self.getLockedVersionFor(req)
	if err != nil {
		return nil, fmt.Errorf("getting locked version: %w", err)
	}

	var latestMajor, latestMinor, latestPatch, latestNotDeprecatedVersion *semver.Version = lockedVersion, lockedVersion, lockedVersion, nil
	var deprecatedMessage string

	for _, v := range info.Versions {
		version := v.Version
		if version.Prerelease() != "" {
			continue
		}
		if version.GreaterThan(latestMajor) {
			latestMajor = version
		}
		if version.Major() == latestMinor.Major() && version.GreaterThan(latestMinor) {
			latestMinor = version
		}
		if version.Major() == latestPatch.Major() && version.Minor() == latestPatch.Minor() && version.GreaterThan(latestPatch) {
			latestPatch = version
		}
		if (latestNotDeprecatedVersion == nil || version.GreaterThan(latestNotDeprecatedVersion)) && v.Deprecated == "" {
			latestNotDeprecatedVersion = version
		}
		if v.Version.Equal(lockedVersion) {
			deprecatedMessage = v.Deprecated
		}
	}

	return &Status{
		CurrentVersion:   lockedVersion,
		LatestVersion:    info.LatestVersion,
		LatestDeprecated: deprecatedMessage,

		LatestPublishedAt: info.PublishedAt,

		LatestMajor:                latestMajor,
		LatestMinor:                latestMinor,
		LatestPatch:                latestPatch,
		LatestNotDeprecatedVersion: latestNotDeprecatedVersion,
	}, nil
}

func (self Resolver) getLockedVersionFor(req Request) (*semver.Version, error) {
	var lockedVersion string
	for _, dep := range self.lockfile.Dependencies {
		if slices.Contains(dep.Labels, req.Alias+"@"+req.RawVersion) {
			lockedVersion = dep.Version
			break
		}
	}
	if lockedVersion == "" {
		return nil, fmt.Errorf("dependency %s@%s not found in yarn.lock", req.Alias, req.RawVersion)
	}
	return semver.NewVersion(lockedVersion)
}
