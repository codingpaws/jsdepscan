package pkgjson

import (
	"encoding/json"
	"io"
)

type File struct {
	Private             bool              `json:"private"`
	Scripts             map[string]string `json:"scripts"`
	Dependencies        map[string]string `json:"dependencies"`
	DevDependencies     map[string]string `json:"devDependencies"`
	BlockedDependencies map[string]string `json:"blockedDependencies"`
	Resolutions         map[string]string `json:"resolutions"`
}

func Parse(reader io.ReadCloser) (pkg *File, err error) {
	err = json.NewDecoder(reader).Decode(&pkg)
	return
}
