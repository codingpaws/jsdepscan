module gitlab.com/codingpaws/jsdepscan

go 1.21.7

require (
	github.com/Masterminds/semver v1.5.0
	github.com/fatih/color v1.16.0
	github.com/rodaine/table v1.1.1
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/gobwas/glob v0.2.3
	github.com/kr/text v0.2.0
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	gitlab.com/codingpaws/yarnlock v0.0.0-20240325000118-653fb758f5f6
	golang.org/x/sys v0.14.0 // indirect
)
