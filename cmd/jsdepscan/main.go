package main

import (
	"fmt"
	"log"
	"os"
	"slices"
	"strings"
	"time"

	"gitlab.com/codingpaws/jsdepscan/internal/config"
	"gitlab.com/codingpaws/jsdepscan/internal/depinfo"
	"gitlab.com/codingpaws/jsdepscan/internal/pkgjson"
	"gitlab.com/codingpaws/yarnlock"

	"github.com/fatih/color"
	"github.com/kr/text"
	"github.com/rodaine/table"
)

var headerFormat = color.New(color.FgGreen, color.Underline).SprintfFunc()
var columnFormat = color.New(color.FgYellow).SprintfFunc()
var conf *config.Config

func main() {
	log.SetFlags(log.LstdFlags | log.LUTC)
	log.SetPrefix("\x1b[33;1m[jsdepscan]\x1b[0m ")
	color.NoColor = false

	var err error
	conf, err = config.LoadConfigFromDefault()
	if err != nil {
		log.Fatalln(err)
	}

	pkgjson, yarnlock, err := readFiles()
	if err != nil {
		log.Fatalln(err)
	}

	resolver := depinfo.NewResolver(yarnlock)

	dependencies := pkgjson.Dependencies
	for k, v := range pkgjson.DevDependencies {
		dependencies[k] = v
	}

	err = run(resolver, dependencies)
	if err != nil {
		log.Fatalln(err)
	}
}

func readFiles() (*pkgjson.File, *yarnlock.File, error) {
	pkgJsonFile, err := os.Open("package.json")
	if err != nil {
		return nil, nil, err
	}

	defer pkgJsonFile.Close()
	pkgjson, err := pkgjson.Parse(pkgJsonFile)
	if err != nil {
		return nil, nil, fmt.Errorf("parsing pkgjson: %w", err)
	}

	yarnlockFile, err := os.Open("yarn.lock")
	if err != nil {
		return nil, nil, fmt.Errorf("opening yarn.lock: %w", err)
	}
	defer yarnlockFile.Close()
	yarnlock, err := yarnlock.Parse(yarnlockFile)
	if err != nil {
		return nil, nil, fmt.Errorf("parsing yarn.lock: %w", err)
	}
	log.Printf("found %d direct, %d dev, and %d transitive.\n", len(pkgjson.Dependencies), len(pkgjson.DevDependencies), len(yarnlock.Dependencies))
	log.Println()

	return pkgjson, yarnlock, nil
}

type SimpleInfo struct {
	alias, version, latestVersion string
	latestPublishedAt             time.Time
	deprecatedReason              string
	latestNotDeprecatedVersion    string
}

func run(resolver *depinfo.Resolver, dependencies map[string]string) error {
	patchUpgrades := []SimpleInfo{}
	minorUpgrades := []SimpleInfo{}
	majorUpgrades := []SimpleInfo{}
	staleDependencies := []SimpleInfo{}
	deprecatedDependencies := []SimpleInfo{}

	total := len(dependencies)
	i := 0

	for alias, version := range dependencies {
		if total/10 > 0 && i%(total/10) == 0 {
			percent := i * 100 / total
			log.Printf("resolving %d%% (%d/%d)...\n", percent, i+1, total)
		}
		i++
		status, err := resolve(resolver, alias, version)
		if err != nil {
			return fmt.Errorf("resolving %s: %w", alias, err)
		}

		latestNotDeprecatedVersion := "<none>"
		if status.LatestNotDeprecatedVersion != nil {
			latestNotDeprecatedVersion = status.LatestNotDeprecatedVersion.String()
		}

		upgradeInfo := SimpleInfo{
			alias:                      alias,
			version:                    status.CurrentVersion.String(),
			latestVersion:              status.LatestVersion.String(),
			latestPublishedAt:          status.LatestPublishedAt,
			deprecatedReason:           status.LatestDeprecated,
			latestNotDeprecatedVersion: latestNotDeprecatedVersion,
		}

		if time.Since(status.LatestPublishedAt) > 365*24*time.Hour {
			staleDependencies = append(staleDependencies, upgradeInfo)
		}

		if status.LatestPatch.GreaterThan(status.CurrentVersion) {
			upgradeInfo.latestVersion = status.LatestPatch.String()
			patchUpgrades = append(patchUpgrades, upgradeInfo)
		}
		if status.LatestMinor.GreaterThan(status.CurrentVersion) {
			upgradeInfo.latestVersion = status.LatestMinor.String()
			minorUpgrades = append(minorUpgrades, upgradeInfo)
		}
		if status.LatestMajor.GreaterThan(status.CurrentVersion) {
			upgradeInfo.latestVersion = status.LatestMajor.String()
			majorUpgrades = append(majorUpgrades, upgradeInfo)
		}
		if status.LatestDeprecated != "" {
			deprecatedDependencies = append(deprecatedDependencies, upgradeInfo)
		}
	}

	sortFunc := func(a, b SimpleInfo) int {
		return strings.Compare(a.alias, b.alias)
	}

	slices.SortFunc(patchUpgrades, sortFunc)
	slices.SortFunc(minorUpgrades, sortFunc)
	slices.SortFunc(majorUpgrades, sortFunc)
	slices.SortFunc(staleDependencies, sortFunc)

	fmt.Println()
	fmt.Println("Stale dependencies (>1yr no release):")
	printStaleDependencies(staleDependencies)

	fmt.Println()
	fmt.Println("Patch upgrades:")
	printUpgrades(patchUpgrades, "patch")

	fmt.Println()
	fmt.Println("Minor upgrades:")
	printUpgrades(minorUpgrades, "minor")

	fmt.Println()
	fmt.Println("Major upgrades:")
	printUpgrades(majorUpgrades, "major")

	fmt.Println()
	fmt.Println("Deprecated:")
	printDeprecated(deprecatedDependencies)

	return nil
}

func printStaleDependencies(staleDependencies []SimpleInfo) {
	if len(staleDependencies) == 0 {
		color.New(color.Faint).Println("None :)")
		return
	}
	var ignores int
	tbl := table.New("Name", "Latest", "Last published", "Weeks ago").WithHeaderFormatter(headerFormat).WithFirstColumnFormatter(columnFormat)
	for _, dep := range staleDependencies {
		if _, ok := conf.Ignores(dep.alias, "stale"); ok {
			ignores++
			continue
		}
		weeksAgo := int(time.Since(dep.latestPublishedAt).Hours() / (24 * 7))
		tbl.AddRow(dep.alias, dep.latestVersion, dep.latestPublishedAt.Format(time.RFC822), weeksAgo)
	}
	tbl.Print()

	if ignores > 0 {
		color.New(color.Faint).Printf("Ignored %d stale dependencies as per rules.\n", ignores)
	}
}

func printDeprecated(deprecated []SimpleInfo) {
	if len(deprecated) == 0 {
		color.New(color.Faint).Println("None :)")
		return
	}
	var ignores int
	tbl := table.New("Name", "Reason", "Replacement").WithHeaderFormatter(headerFormat).WithFirstColumnFormatter(columnFormat)
	for _, dep := range deprecated {
		if _, ok := conf.Ignores(dep.alias, "deprecated"); ok {
			ignores++
			continue
		}
		reason := text.Wrap(dep.deprecatedReason, 50)
		tbl.AddRow(dep.alias, dep.latestNotDeprecatedVersion, reason)
	}
	tbl.Print()
	if ignores > 0 {
		color.New(color.Faint).Printf("Ignored %d deprecated dependencies as per rules.\n", ignores)
	}
}

func printUpgrades(info []SimpleInfo, category string) {
	if len(info) == 0 {
		color.New(color.Faint).Println("None :)")
		return
	}

	var ignores int
	tbl := table.New("Name", "Current", "Latest").WithHeaderFormatter(headerFormat).WithFirstColumnFormatter(columnFormat)
	for _, dep := range info {
		if _, ok := conf.Ignores(dep.alias, category); ok {
			ignores++
			continue
		}
		tbl.AddRow(dep.alias, dep.version, dep.latestVersion)
	}
	tbl.Print()
	if ignores > 0 {
		color.New(color.Faint).Printf("Ignored %d %s upgrades as per rules.\n", ignores, category)
	}
}

func resolve(resolver *depinfo.Resolver, alias, version string) (*depinfo.Status, error) {
	packageName := alias
	rawVersion := version

	if strings.HasPrefix(version, "npm:") {
		version = version[4:]
		versionSignIdx := strings.LastIndex(version, "@")
		if versionSignIdx == -1 {
			log.Fatalf("%s: invalid version %s", alias, version)
		}
		packageName = version[:versionSignIdx]
		version = version[versionSignIdx+1:]
	}
	return resolver.Resolve(depinfo.Request{
		Alias:           alias,
		PackageName:     packageName,
		SelectedVersion: version,
		RawVersion:      rawVersion,
	})
}
